#!/usr/bin/env python

import rospy
import roslib
import actionlib
import math
from multiple_robot_gazebo.msg import HumanGazebo, Human, HumanArray
from multiple_robot_gazebo.srv import UpdateHumanMarkerState, GetAllHumanData
from multiple_robot_nav.msg import HandleGoalAction, HandleGoalGoal, HandleGoalResult
from move_base_msgs.msg import MoveBaseGoal
from nav_msgs.msg import Odometry

class AGVWithTask:
    def __init__(self, name, action_clinet, target_human=-1, position_x = 0.0, position_y=0.0):
        self.name = name
        self.action_clinet = action_clinet
        self.target_human = target_human
        self.position_x = position_x
        self.position_y = position_y

class SimpleHuman:
    def __init__(self, id, type=0, state = 0, serviceAGV = None, position_x=0.0, position_y=0.0):
        self.id = id
        self.state = state
        self.type = type
        self.serviceAGV = serviceAGV
        self.position_x = position_x
        self.position_y = position_y

agvs_with_task_list = {}
simple_human_list = {}
manager_lock = False
start_time = None
finish_time = None

def updateAGVPosition(data):
    global agvs_with_task_list
    agv_name = data.header.frame_id.split('/')[0]
    agvs_with_task_list[agv_name].position_x = data.pose.pose.position.x
    agvs_with_task_list[agv_name].position_y = data.pose.pose.position.y

def getAllHumanList():
    global simple_human_list
    simple_human_list = {}
    get_all_human_data = rospy.ServiceProxy('get_all_human_data', GetAllHumanData)
    human_array = get_all_human_data().human_array
    for human in human_array.humans:
        simpleHuman = SimpleHuman(id=human.id, 
                                type=human.type, 
                                state=human.state,
                                position_x = human.marker.pose.position.x,
                                position_y = human.marker.pose.position.y)
        simple_human_list[human.id] = simpleHuman

def initializeAGVList():
    global agvs_with_task_list
    agvs_with_task_list = {}
    all_topic = rospy.get_published_topics()
    needed_topic = [topic[0] for topic in all_topic if "odom" in topic[0]]
    for topic in needed_topic:
        name = topic.split('/')
        name = filter(None, name)
        action_clinet = actionlib.SimpleActionClient("/" + name[0] + '/handle_goal', HandleGoalAction)
        action_clinet.wait_for_server()
        agvs_with_task_list[name[0]] = AGVWithTask(name=name[0], action_clinet=action_clinet, target_human=-1)
        rospy.Subscriber(topic, Odometry, updateAGVPosition)
    rospy.logwarn("Get %i AGVs." % (len(needed_topic)))

def getTheShortestTarget(agvName):
    global agvs_with_task_list
    humanId = -1
    distance = 999999
    for key, human in simple_human_list.items():
        if human.serviceAGV is not None:
            continue
        dx_pow = (human.position_x - agvs_with_task_list[agvName].position_x) ** 2
        dy_pow = (human.position_y - agvs_with_task_list[agvName].position_y) ** 2
        _dis = math.sqrt(dx_pow + dy_pow)
        if _dis < distance:
            distance = _dis
            humanId = human.id
    return humanId

def doneCb(state, result):
    global manager_lock, agvs_with_task_list, simple_human_list
    agvName = result.agv_name
    state = int(agvName[-1])
    rospy.logwarn("%s done." % agvName)
    serviced_human_id = agvs_with_task_list[agvName].target_human
    update_human_marker_state = rospy.ServiceProxy('update_human_marker_state', UpdateHumanMarkerState)
    update_human_marker_state(id=serviced_human_id, state=state)
    try :
        del simple_human_list[serviced_human_id]
    except Exception as e:
        rospy.loginfo(e)

    while manager_lock:
        continue
    
    manager_lock = True
    
    nextHumanId = getTheShortestTarget(agvName)
    if nextHumanId == -1:
        return
    agvs_with_task_list[agvName].target_human = nextHumanId
    try :
        simple_human_list[nextHumanId].serviceAGV = agvName
    except Exception as e:
        rospy.loginfo(e)

    goal = HandleGoalGoal()
    goal.target_pose.header.frame_id = agvName + "/odom"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position.x = simple_human_list[nextHumanId].position_x
    goal.target_pose.pose.position.y = simple_human_list[nextHumanId].position_y
    goal.target_pose.pose.orientation.w = 1.0
    goal.start_pose.pose.position.x = agvs_with_task_list[agvName].position_x
    goal.start_pose.pose.position.y = agvs_with_task_list[agvName].position_y
    action_clinet = agvs_with_task_list[agvName].action_clinet
    action_clinet.send_goal(goal, done_cb=doneCb)
    
    manager_lock = False

if __name__ == '__main__':
    rospy.init_node("patrol_task_manager")

    # Wait for get_all_human_data service
    rospy.loginfo('Wait for get_all_human_data service')
    rospy.wait_for_service('get_all_human_data')
    rospy.loginfo('OK')

    # Initialize humanArray
    getAllHumanList()
    # Initialize AGVList
    while len(agvs_with_task_list) < 1:
        initializeAGVList()
    rospy.sleep(3)

    start_time = rospy.Time.now()
    for key, value in agvs_with_task_list.items():
        handleGoalResult = HandleGoalResult(agv_name=value.name)
        doneCb(None,handleGoalResult)

    while len(simple_human_list) > 0:
        continue
    finish_time = rospy.Time.now()
    cost_time = finish_time.to_sec() - start_time.to_sec()
    rospy.logwarn("Cost time: %f" % (cost_time))
    
    # rospy.spin()
    
    
