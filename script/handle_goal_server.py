#!/usr/bin/env python
import rospy
import copy
import math
import sys
import roslib
import actionlib
import multiple_robot_nav.msg
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseFeedback

class HandleGoalAction(object):
    _feedback = multiple_robot_nav.msg.HandleGoalFeedback(agv_name=rospy.get_namespace().translate(None, '/'))
    _result = multiple_robot_nav.msg.HandleGoalResult(agv_name=rospy.get_namespace().translate(None,'/'))
        
    def __init__(self, name, deviation_with_target, obstacle_radius):
        # Initialize move_base_action needed parameter
        # Connect move_base_action
        self._prefix = rospy.get_namespace()
        self._client = actionlib.SimpleActionClient(self._prefix + 'move_base', MoveBaseAction)
        self._client.wait_for_server()
        self._angle_with_target = 0
        self._stuck_count = 0
        self._stuck_count_max = 3
        # Initialize HandleGoalAction parameters 
        self._action_name = name 
        self._deviation_with_target = deviation_with_target
        self._now_distance = 999999.0
        self._last_distance = 999999.0
        self._obstacle_radius = obstacle_radius

        # Create HandleGoalAction Object
        self._as = actionlib.SimpleActionServer(self._action_name, 
                            multiple_robot_nav.msg.HandleGoalAction,
                            execute_cb=self.execute_cb, auto_start=False )
        # Start HandleGoalAction
        rospy.loginfo('%s: launching.' % (self._action_name))
        self._as.start()


    # HandleGoalAction execute callback
    def execute_cb(self, goal):
        rospy.loginfo("%s sended." % rospy.get_namespace().translate(None,'/'))
        r = rospy.Rate(4)
        success = True
        self._start_x = goal.start_pose.pose.position.x
        self._start_y = goal.start_pose.pose.position.y
        self._target_x = goal.target_pose.pose.position.x # the position_x of human or others target
        self._target_y = goal.target_pose.pose.position.y # the position_y of human or others target
        self._angle_with_target = math.atan2(self._start_y - goal.target_pose.pose.position.y, self._start_x - goal.target_pose.pose.position.x) #(rad)
        converted_goal = self.convert_goal_position(goal)
        self._now_distance = 999999.0
        self._feedback.distance = self._now_distance
        # publish info to the console for the user
        rospy.loginfo('%s: Executing, target_pose is (x:%i, y:%i),  distance initialize by %i' % (self._action_name, self._target_x, self._target_y, self._feedback.distance))
        self._client.send_goal(converted_goal, feedback_cb=self.feedbackCb)

        while self._now_distance > self._deviation_with_target:
            if self._as.is_preempt_requested():
                rospy.logerr('%s: Preempted' % self._action_name) 
                self._as.set_preempted()
                success = False
                break
            if self.is_stuck():
                # rospy.logerr(self._action_name + " stuck!!!!!!!!")
                self._client.cancel_goal()
                converted_goal = self.convert_goal_position(goal)
                self._client.send_goal(converted_goal, feedback_cb=self.feedbackCb)

            rospy.loginfo('%s: Executing, distance from target point is %f' % (self._action_name, self._now_distance))
            # self._as.publish_feedback(self._feedback)
            r.sleep()
        if success:
            self._client.cancel_all_goals()
            rospy.sleep(1)
            rospy.loginfo('%s: Successed' % self._action_name)
            self._as.set_succeeded(self._result)
        self.init_param()

    def feedbackCb(self, feedback):
        dx_pow = (self._goal_x - feedback.base_position.pose.position.x) ** 2
        dy_pow = (self._goal_y - feedback.base_position.pose.position.y) ** 2
        self._last_distance = self._now_distance
        self._now_distance = math.sqrt(dx_pow + dy_pow)
        self._feedback.distance = self._now_distance

#math.sin(math.radians(90))
    def convert_goal_position(self, goal):
        rospy.logerr(self._angle_with_target)
        convert_goal = MoveBaseGoal()
        goal_x = goal.target_pose.pose.position.x + math.cos(self._angle_with_target) * self._obstacle_radius
        goal_y = goal.target_pose.pose.position.y + math.sin(self._angle_with_target) * self._obstacle_radius
        self._goal_x = goal_x
        self._goal_y = goal_y
        convert_goal.target_pose.header.frame_id = goal.target_pose.header.frame_id
        convert_goal.target_pose.header.stamp = rospy.Time.now()
        convert_goal.target_pose.pose.orientation.w = goal.target_pose.pose.orientation.w
        convert_goal.target_pose.pose.position.x = goal_x
        convert_goal.target_pose.pose.position.y = goal_y
        return convert_goal

    def is_stuck(self):
        if self._last_distance - self._now_distance < 0.01:
            self._stuck_count = self._stuck_count + 1
        else:
            self._stuck_count = 0

        if self._stuck_count >= self._stuck_count_max:
            self._stuck_count = 0
            self._angle_with_target = self._angle_with_target + 0.157 #(rad)
            return True
        else:
            return False

    def init_param(self):
        self._error_count = 0
        self._stuck_count = 0

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("USAGE: handleGoal.py [distance]")
        exit(0)

    # Initialize needed parameters
    deviation_with_target = float(sys.argv[1])
    obstacle_radius = float(sys.argv[2])
    print(rospy.get_namespace())
    # Initialize ros node
    rospy.init_node('handle_goal')
    server= HandleGoalAction(name= rospy.get_name(), deviation_with_target=deviation_with_target, obstacle_radius=obstacle_radius)
    rospy.spin()

