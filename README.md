# 專案預覽圖

兩台AGV協作達到巡視每個人形物件
![alt text](img/gazebo.png "gazebo")
被巡檢過的人形物件在rviz中會變成對應的顏色：被AGV1巡檢過會變為綠色，AGV2則是藍色。
![alt text](img/rviz.png "rviz")

# Demo
需要先執行multiple_robot_gazebo的Demo，將模擬環境建好，並將人行物件廣播至Topic。
1. Launch navigation with rviz.

        $ roslaunch multiple_robot_nav navigation_in_stage1_world.launch
2. Run the patrol node.

        $ rosrun multiple_robot_nav patrol_task_manager.py  

# 專案內容簡述
專案目標為開發協作的服務型AGV，在會場或餐廳可使多台AGV達到多工的送餐、送水服務，為模擬展示可搭配multiple_robot_gazebo專案做使用。

# 檔案結構
- __action：__
    放置action所需要的資料結構
- __img：__
    僅放置本專案展示圖片用。
- __launch：__
    放置因需求不同而結合不同Node的launch file。
- __maps：__
    放置預先掃描(slam)完地圖檔案，啟動map_server時需要載入使用的地圖。
- __msg：__
    放置自定義Topic資料結構的定義檔。
- __param：__
    放置move_base中所需要的各種演算法參數。
- __rviz：__
    放置rviz介面所儲存的欄位參數，AGV資訊、地圖資訊、路徑、障礙物等，可使下一次打開rviz時就已有需要資訊。
- __script：__
    放置Python程式碼的資料夾，其功能通常都為ROS的Node
- __src：__
    與script性質一樣，差別在於程式碼是使用C++撰寫，本專案使用Python所以沒有該資料夾

# 開發重點步驟
1. 使AGV的navigation功能正常運作
2. 撰寫符合目的之執行元件及管理元件
3. 研究如何協作可提昇效率及穩定度

# 各步驟重點及細節
本專案相較於multiple_robot_gazebo的建製環境還要複雜許多，接下來會盡量解釋各步驟的重點及目的。

## Navigation功能實現
(官方參考資料：http://wiki.ros.org/navigation/Tutorials/RobotSetup)

(其他參考資料：https://www.guyuehome.com/267)

實現Navigation前，先了解官方提供的架構圖：
![alt text](img/navigation_architecture.png "navigation_architecture")

其中白色、灰色框內的元件ROS以提供相關套件，藍色框內則是需要自行撰寫於AGV裡頭，以下為各元件的功能介紹。

- __amcl:__(於multiple_robot_gazebo中的gazebo.xacro中有建立模擬資訊)

    蒙地卡羅自適應定位元件，透過讀取光達資訊，修正AGV的座標。

- __sensor transforms:__

    光達的座標變換元件，因光達並不會在機器人中央，所以需要經過tf將光達座標與機器人的座標做轉換，這部份於multiple_robot_gazebo專案中，在建立robort_description時就以處理完畢，AGV的描述檔urdf就以描述各元件相關位置，經過"robot_state_publisher"傳出即可。

- __odometry source:__(於multiple_robot_gazebo中的gazebo.xacro中有建立模擬資訊)

    先前的robort_description只存在AGV的位置等資訊，不包含速度資訊，所以需要對robort_description的位置資訊做計算，得到單位時間的距離變化後在傳至指定Topic，通常為xx/odom的topic。

    (參考資料：
    1. http://wiki.ros.org/navigation/Tutorials/RobotSetup/Odom

    2. https://www.guyuehome.com/332
    )

- __sensor source:__(於multiple_robot_gazebo中的gazebo.xacro中有建立模擬資訊)

    此元件單純接收來自AGV光達的資訊即可。
    
- __map_server:__
    
    最單純的元件，僅將掃描好的地圖檔載入map_server並啟動即可。

- __base controller:__(於multiple_robot_gazebo中的gazebo.xacro中有建立模擬資訊)
    
    AGV上的馬達控制程式，接收move_base的指令後，達到相對應的移動速度。

- __move_base:__

    navigation中的核心元件，使AGV達到規劃路徑、避障等功能。

## 實現方法
接下來將以範例介紹如何實現Navigation，大重點為：
1. amcl的實做
2. move_base的實做
3. 結合amcl, movebase, map_server

範例程式可參考launch資料夾底下的amcl.launch, move_base.launch, navigation.launch三個檔案。

## 1. amcl實做
此部份將以launch資料夾底下的amcl.launch做為參考，進入配置內容前先提醒一個重要的細節：

`frame_id`：我們都知道ROS最一開始的介紹是以Topic作為資料傳輸的媒介，除了Topic的名稱外，還有`frame_id`需要注意，Topic名稱對了但frame_id不正確，也是無法收到資料,且ROS在某次更新後frame_id開頭是不能有`"/"`的（topic前會有"/"為前綴），接下來的內容會經常看到這兩者的配置。

`本專案以多台AGV為基礎，若使用單台AGV且使用turtlebot相關範例程式，會有問題，請自行修改frame_id, Topic name的名稱，移除prefix以及多餘"/"。`

amcl.launch的配置單純些，僅執行amcl套件的amcl node，並將參數整優化即可：

__3行__：use_map_topic設置為"false"，可使用預設的"/map"，若有指定特殊的map_server所發布的地圖資訊也可做修改。

__4~6行__：設定AGV的預設座標。
    
- x: x座標
- y: y座標
- a: 角度

__7行__： 用來區別多台AGV的前綴。

_(3~7行獨立出來的arg目的其一為方便管理，其一為讓引用者可更改參數)_

__10~48行__：為amcl的核心元件，直接只用ros提供的amcl套件即可，以下為幾個特別的參數修改，其餘的可以至官方參考：

- < param name="odom_frame_id" /> ：因原amcl是以單台AGV為例，所以需要在讀取odom(odometry source)的Topic加上前綴。

`（提醒：在multiple_robot_gazebo中使用group區分AGV1, AGV2，會使Topic與frame_id等名稱都加上group名稱當做前綴。）`

- < param name="base_frame_id" /> ：base_frame為AGV狀態所發布的frame_id，可參照multiple_robot_gazebo中ROS及Gazebo的描述檔。

- < remap from="scan" />：與上面兩者類似，但topic需要使用remap重新導向，將原先訂閱scan（名稱不需加上`"\"`）的服務導向訂閱指定的Topic（名稱需要加上`"\"`）。

(官方參考資料：http://wiki.ros.org/amcl)

(其他參考資料：https://www.guyuehome.com/273)

## 2. move_base實做
此部份參考launch資料夾底下的move_base.launch，一樣直接使用ROS所提供的move_base套件即可,配置內容有兩重點：
1. 配置檔的引入。
2. frame_id與Topic的修正。

- ### 配置檔的引入
    內容為9~15行為配置檔的引入，引入的檔案都可在param資料夾中查找，除了move_base本身的一些參數外，其他可分為`global`及`local`兩大類，而內容各自都含有`cost_map`及`planner`。
    
    - cost_map：計算障礙物與AGV的碰撞可能，於官方文件在move_base沒有太詳細說明，視為隱藏的Parameters，而分別為`global_costmap/`及`local_costmap/`。\
    (部份資料可參考：http://wiki.ros.org/costmap_2d)

    - planner：規劃路徑的元件，其中也包含了自動避障的功能。

    首先關於ROS提供的planner有非常多可以選擇，但ROS的官方並沒有明確的文件，以下稍微整理幾個可使用的planner：

    - global：

        - [navfn/NavfnROS](http://wiki.ros.org/navfn) (default)
        - [global_planner/GlobalPlanner](http://wiki.ros.org/global_planner) (推薦使用該套件之A*演算法)

    - local：

        - [base_local_planner/TrajectoryPlannerROS](http://wiki.ros.org/base_local_planner) (default)
        - [dwa_local_planner/DWAPlannerROS](http://wiki.ros.org/dwa_local_planner)
        - [teb_local_planner/TebLocalPlannerROS](http://wiki.ros.org/teb_local_planner) (推薦)

    __9行__：將local_planner使用的套件更換為DWAPlannerROS。

    __10~15行__：將各cost_map及planner的參數匯入，注意cost_map通常配置大多相同，所以在10~11行匯入相同檔案，並訂到不同的name_space底下。

    各配置檔內容可至相關連接中查詢，`需要注意的是param資料夾中的配置檔若有跟Topic及frame_id相關的配置需要在另外修改，單台AGV可略過。`

    (官方參考資料：http://wiki.ros.org/move_base)

- ### frame_id與Topic的修正

    此步驟跟amcl中的更改一樣，將frame_id加上前綴修正，而部份Topic並非本move_base所使用的，無法使用remap重新導向，需要將參數更改掉，如19行的"global_costmap/scan/topic"，記得Topic最前面需要補上`"/"`。

## 3. 整合實做
最後一個步驟可參考launch資料夾底下的navigation.launch，把map_server加入，並且依所需將各自AGV的amcl與move_base建立出即可。

__1~6行__：所需參數配置，首先將地圖檔的路徑定義好，並加入AGV名稱以及其他所需參數。

__9~11行__：map_server的建立，直接使用ROS提供的map_server套件即可，將事先定義好的地圖檔案路徑帶入args，並將frame_id改為map就好了。

__13~23行__：因為需要多台的AGV，所以使用group將amcl與move_base群組起來，並使用AGV的名稱當作group的name space。其中可能會有疑問，已經用group群組起來了，那為何在先前的步驟還要更改topic與frame_id，因為先前是讀取配置檔的內容，並不會受到group的影響，所以需要在另外重新更改。

`完成以上配置後，便可照Demo執行各服務，以及可在rviz中使用"2D Nav Goal"來設定目的地讓AGV自行前往`

---

## 依需求自訂Action_server

# Updating...